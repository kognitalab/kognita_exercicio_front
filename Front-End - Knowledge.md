### Informações importantes:

- Data ideal de entrega: 1 semana a partir da data de recebimento.
- O exercício é de execução individual.
- O candidato deverá subir suas respostas em um repositório (exemplos: github, gitlab etc) e disponibilizar o link para a Kognita.
- O questionário não tem caráter eliminatório, apenas classificatório.
- Qualquer dúvida, não hesite em entrar em contato.


### Questões de JS:
1- Quais tecnologias de front-end tem conhecimento? Correlacione as mesmas com os tipos de projetos que julga adequado sua utilização:

<br/><br/>

2- Como você testa seu JavaScript? Framework, forma de testar, etc

<br/><br/>

3- Como você organiza seu código? (module pattern, herança clássica?)

<br/><br/>

4- Quando você otimiza seu código?

<br/><br/>

5- Você já utilizou templates com Javascript?
Se sim, quais bibliotecas foram utilizadas? (Mustache.js, Handlebars, etc.)

<br/><br/>


### Questões de HTML:
1- Cite boas práticas que costuma usar nos seus códigos html:

<br/><br/>

2- Como você desenvolve uma página com conteúdo em múltiplos idiomas?

<br/><br/>

3- Que tipo de coisas que você deve tomar cuidado ao desenvolver um website multi-língua?

<br/><br/>

4- Descreva a diferença entre cookies, sessionStorage e localStorage.

<br/><br/>

5- Descreva a diferença entre GET e POST

<br/><br/>